package callambuckman1072819.com.receiptme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import callambuckman1072819.com.receiptme.database.ReceiptBaseHelper;
import callambuckman1072819.com.receiptme.database.ReceiptCursorWrapper;
import callambuckman1072819.com.receiptme.database.ReceiptDB.ReceiptTable;

public class ListUi {
    private static ListUi sListUi;

    //private List<Receipt>  mReceipts;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static ListUi get(Context context)
    {
        if(sListUi == null)
        {
            sListUi = new ListUi(context);
        }
        return sListUi;
    }
    private static ContentValues getContentValues(Receipt receipt)
    {
        ContentValues values = new ContentValues();
        values.put(ReceiptTable.Cols.UUID, receipt.getId().toString());
        values.put(ReceiptTable.Cols.TITLE, receipt.getTitle());
        values.put(ReceiptTable.Cols.DATE, receipt.getDate().getTime());
        values.put(ReceiptTable.Cols.SHOPNAME, receipt.getShopName());
        return values;
    }
    public void updateReceipt(Receipt receipt)
    {
        String uuidString = receipt.getId().toString();
        ContentValues values = getContentValues(receipt);

        mDatabase.update(ReceiptTable.NAME, values, ReceiptTable.Cols.UUID + "= ?", new String[] {uuidString});
    }

    private ReceiptCursorWrapper queryReceipt(String whereClause, String[] whereArgs)
    {
        Cursor cursor = mDatabase.query(
                ReceiptTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null

        );
        return new ReceiptCursorWrapper(cursor);
    }
    private ListUi(Context context)
    {
        mContext = context.getApplicationContext();
        mDatabase = new ReceiptBaseHelper(mContext).getWritableDatabase();
        //mReceipts = new ArrayList<>();
    }
    public void addCrime(Receipt r) {
        //mReceipts.add(r);
        ContentValues values = getContentValues(r);
        mDatabase.insert(ReceiptTable.NAME, null, values);
    }
    public void deleteReceipt(Receipt r) {
       // mReceipts.remove(r);
        ContentValues values = getContentValues(r);
        String uuidString = r.getId().toString();
        mDatabase.delete(ReceiptTable.NAME, ReceiptTable.Cols.UUID + "= ?",new String[] {uuidString});
    }
    public File getPhotoFile(Receipt receipt)
    {
        File filesDir = mContext.getFilesDir();
        return new File(filesDir, receipt.getPhotoFilename());
    }
    public List<Receipt> getReceipts()
    {
        //return mReceipts;
       // return new ArrayList<>();
        List<Receipt>receipts = new ArrayList<>();
        ReceiptCursorWrapper cursor = queryReceipt(null, null);
        try {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                receipts.add(cursor.getReceipt());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return receipts;
    }

    public Receipt getReceipt(UUID id)
    {

       ReceiptCursorWrapper cursor = queryReceipt(
               ReceiptTable.Cols.UUID + " = ?",
               new String[]{id.toString()}
       );
       try {
           if (cursor.getCount() == 0){
               return null;
           }
           cursor.moveToFirst();
           return cursor.getReceipt();

       }finally {
           cursor.close();
       }

    }

}
