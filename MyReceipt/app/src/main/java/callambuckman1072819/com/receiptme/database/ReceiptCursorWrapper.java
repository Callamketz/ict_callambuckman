package callambuckman1072819.com.receiptme.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Date;
import java.util.UUID;

import callambuckman1072819.com.receiptme.Receipt;

public class ReceiptCursorWrapper extends CursorWrapper {
    public ReceiptCursorWrapper(Cursor cursor)
    {
        super(cursor);
    }
    public Receipt getReceipt()
    {
        String uuidString = getString(getColumnIndex(ReceiptDB.ReceiptTable.Cols.UUID));
        String title = getString(getColumnIndex(ReceiptDB.ReceiptTable.Cols.TITLE));
        long date = getLong(getColumnIndex(ReceiptDB.ReceiptTable.Cols.DATE));
        String shopName = getString(getColumnIndex(ReceiptDB.ReceiptTable.Cols.SHOPNAME));

        Receipt receipt = new Receipt(UUID.fromString(uuidString));
        receipt.setTitle(title);
        receipt.setShopName(shopName);
        receipt.setDate(new Date(date));
        return receipt;
    }
}
