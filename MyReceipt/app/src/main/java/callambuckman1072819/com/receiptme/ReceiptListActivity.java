package callambuckman1072819.com.receiptme;


import android.app.Fragment;

public class ReceiptListActivity extends SingleFragmentActivity {

    @Override
    protected android.support.v4.app.Fragment createFragment()
    {
        return new ReceiptListFragment();
    }

}
