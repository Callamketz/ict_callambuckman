package callambuckman1072819.com.receiptme;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.AppCompatDrawableManager.get;

public class ReceiptListFragment extends android.support.v4.app.Fragment {
    private RecyclerView mReceiptRecycleView;
    private ReceiptAdapter mReceiptAdapter;
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_receipt_list, container, false);
        setHasOptionsMenu(true);
        mReceiptRecycleView =(RecyclerView) view.findViewById(R.id.receipt_recyclerView);
        mReceiptRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return view;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        updateUI();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_receipt_list, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_crime:
                Receipt receipt = new Receipt();
                ListUi.get(getActivity()).addCrime(receipt);
                Intent i = new Intent(getActivity(), ReceiptActivity.class);
                i.putExtra(ReceiptActivity.EXTRA_RECEIPT_ID, receipt.getId());

                startActivityForResult(i, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI()
    {
        ListUi listUi = ListUi.get(getActivity());
        List<Receipt> receipts = listUi.getReceipts();
        if(mReceiptAdapter == null){
            mReceiptAdapter = new ReceiptAdapter(receipts);
            mReceiptRecycleView.setAdapter(mReceiptAdapter);
        }
        else
        {
            mReceiptAdapter.setReceipts(receipts);
            mReceiptAdapter.notifyDataSetChanged();
        }

    }
    private class ReceiptHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mShopNameTextView;

        public ReceiptHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.list_item_receipt, parent, false));
            mTitleTextView = (TextView) itemView.findViewById(R.id.item_receipt_title);
            mDateTextView = (TextView) itemView.findViewById(R.id.item_receipt_date);
            mShopNameTextView = (TextView) itemView.findViewById(R.id.item_receipt_shopName);
            itemView.setOnClickListener(this);
        }
        private Receipt mReceipt;
        public void bind(Receipt receipt)
        {
            mReceipt = receipt;
            mTitleTextView.setText(mReceipt.getTitle());
            mDateTextView.setText(mReceipt.getDate().toString());
            mShopNameTextView.setText(mReceipt.getShopName());
        }
        @Override
        public void onClick(View view)
        {
            Intent intent = ReceiptActivity.newIntent(getActivity(), mReceipt.getId());
            startActivity(intent);
        }
    }
    private class ReceiptAdapter extends RecyclerView.Adapter<ReceiptHolder>
    {

        private List<Receipt> mReceipt;

        public ReceiptAdapter(List<Receipt> receipts)
        {
            mReceipt = receipts;
        }
        @Override
        public ReceiptHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new ReceiptHolder(layoutInflater, parent);
        }
        @Override
        public void onBindViewHolder(ReceiptHolder holder, int position)
        {
            Receipt receipt = mReceipt.get(position);
            holder.bind(receipt);
        }
        @Override
        public int getItemCount(){
            return mReceipt.size();
        }
        public void setReceipts(List<Receipt> receipts)
        {
            mReceipt = receipts;
        }
    }



}
