package callambuckman1072819.com.receiptme;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import java.util.UUID;

public class ReceiptActivity extends SingleFragmentActivity {

    public static final String EXTRA_RECEIPT_ID = "callambuckman1072819.com.receiptme.receipt_id";

    public static Intent newIntent(Context packageContext, UUID receiptId){
        Intent intent = new Intent(packageContext, ReceiptActivity.class);
        intent.putExtra(EXTRA_RECEIPT_ID, receiptId);
        return intent;
    }
    @Override
    protected Fragment createFragment()
    {

        //return new ReceiptFragment();
        UUID receiptId = (UUID) getIntent().getSerializableExtra(EXTRA_RECEIPT_ID);
        return ReceiptFragment.newInstance(receiptId);
    }

}