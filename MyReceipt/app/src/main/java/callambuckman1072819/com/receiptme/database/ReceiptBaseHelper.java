package callambuckman1072819.com.receiptme.database;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ReceiptBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "receiptBase.db";

    public ReceiptBaseHelper(Context context)
    {
        super(context,DATABASE_NAME, null,VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + ReceiptDB.ReceiptTable.NAME + "(" + "_id integer primary key autoincrement, " +
                ReceiptDB.ReceiptTable.Cols.UUID + ", " +
                ReceiptDB.ReceiptTable.Cols.TITLE + ", " +
                ReceiptDB.ReceiptTable.Cols.DATE + ", " +
                ReceiptDB.ReceiptTable.Cols.SHOPNAME + ")"

        );

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

}
