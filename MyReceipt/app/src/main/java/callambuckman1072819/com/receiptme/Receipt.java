package callambuckman1072819.com.receiptme;

import android.media.Image;

import java.util.Date;
import java.util.UUID;

public class Receipt {
    private UUID mId;
    private String mTitle;
    private String mShopName;
    private Date mDate;
    private Image mImage;
    private String mComment;

    public Receipt() {
       this(UUID.randomUUID());
        //mDate = new Date();
    }
    public Receipt(UUID id)
    {
        mId=id;
        mDate = new Date();
    }
    public String getPhotoFilename(){
        return "IMG_ " + getId().toString() + ".jpg";
    }
    public UUID getId()
    {
        return mId;
    }
    public Image getImage()
    {
        return mImage;
    }
    public void setImage(Image image)
    {
        mImage = image;
    }
    public String getShopName()
    {
        return mShopName;
    }
    public String getComment()
    {
        return mComment;
    }
    public void setComment(String comment)
    {
        mComment = comment;
    }
    public String getTitle()
    {
        return mTitle;
    }
    public Date getDate()
    {
        return mDate;
    }
    public void setDate(Date date)
    {
        mDate = date;
    }
    public void setTitle(String title)
    {
        mTitle = title;
    }
    public void setShopName(String shopName)
    {
        mShopName = shopName;
    }

}