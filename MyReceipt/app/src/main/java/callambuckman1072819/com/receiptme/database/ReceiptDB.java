package callambuckman1072819.com.receiptme.database;

public class ReceiptDB {
    public static final class ReceiptTable{
        public static final String NAME = "receipts";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String SHOPNAME= "shopname";
        }
    }

}
